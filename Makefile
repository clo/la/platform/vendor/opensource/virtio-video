# SPDX-License-Identifier: GPL-2.0-only

KBUILD_OPTIONS+= VIDEO_ROOT=$(KERNEL_SRC)/$(M)

VIDEO_ROOT=$(KERNEL_SRC)/$(M)
VIDEO_COMPILE_TIME = $(shell date)
VIDEO_COMPILE_BY = $(shell whoami | sed 's/\\/\\\\/')
VIDEO_COMPILE_HOST = $(shell uname -n)
VIDEO_COMPILE_TIP = $(shell git log --oneline --no-merges | head -n 1 | tr -d '\"')
VIDEO_GEN_PATH = $(VIDEO_ROOT)/driver/video_generated.h
VIDC_HW_VIRT_HEADER = $(VIDEO_ROOT)/include/vidc_hw_virt.h
KERNEL_VIDC_PATH = $(KERNEL_SRC)/include

all: copy_vidc_hw modules

copy_vidc_hw:
	@cp -av $(VIDC_HW_VIRT_HEADER) $(KERNEL_VIDC_PATH)

$(VIDEO_GEN_PATH): $(shell find . -type f \( -iname \*.c -o -iname \*.h -o -iname \*.mk \))
	echo '#define VIDEO_COMPILE_TIME "$(VIDEO_COMPILE_TIME)"' > $(VIDEO_GEN_PATH)
	echo '#define VIDEO_COMPILE_BY "$(VIDEO_COMPILE_BY)"' >> $(VIDEO_GEN_PATH)
	echo '#define VIDEO_COMPILE_HOST "$(VIDEO_COMPILE_HOST)"' >> $(VIDEO_GEN_PATH)
	echo '#define VIDEO_COMPILE_TIP "$(VIDEO_COMPILE_TIP)"' >> $(VIDEO_GEN_PATH)

modules: $(VIDEO_GEN_PATH)
	$(MAKE) -C $(KERNEL_SRC) M=$(M) modules $(KBUILD_OPTIONS)

modules_install:
	$(MAKE) INSTALL_MOD_STRIP=1 -C $(KERNEL_SRC) M=$(M) modules_install

%:
	$(MAKE) -C $(KERNEL_SRC) M=$(M) $@ $(KBUILD_OPTIONS)

clean:
	rm -f *.o *.ko *.mod.c *.mod.o *~ .*.cmd Module.symvers
	rm -rf .tmp_versions
